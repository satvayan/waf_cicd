module "f5-rs-aws-tgw-network" {
  source       = "git::https://gitlab.com/f5-examples/f5-examples-aws.git//terraform_modules/aws-tgw-net-tf"
#  source       = "../f5-examples-aws/terraform_modules/aws-tgw-net-tf"
  az1          = var.az1
  az2          = var.az2
  name         = var.deployment_name
  aws_region   = "us-west-2"
  username     = var.USERNAME
  public_key   = var.PUBLIC_KEY
  providers = {
    aws     = aws
  }  
}

module bigip {
  source       = "git::https://gitlab.com/f5-examples/f5-examples-aws.git//terraform_modules/aws-bigip"
#  source = "../f5-examples-aws/terraform_modules/aws-bigip"
  prefix                              = "bigip-a"
  f5_instance_count                   = 1
  ec2_key_name                        = module.f5-rs-aws-tgw-network.key_name
  mgmt_subnet_security_group_ids      = [module.f5-rs-aws-tgw-network.sec-group-vpc-1-ssh-icmp, module.f5-rs-aws-tgw-network.vpc-1-sec-group-vpc-1-web]
  public_subnet_security_group_ids    = [module.f5-rs-aws-tgw-network.vpc-1-sec-group-vpc-1-web]
  private_subnet_security_group_ids   = [module.f5-rs-aws-tgw-network.vpc-1-sec-group-vpc-1-web]
  vpc_mgmt_subnet_ids                 = [module.f5-rs-aws-tgw-network.vpc-1-mgmt-subnets-ids[0]]
  vpc_public_subnet_ids               = [module.f5-rs-aws-tgw-network.vpc-1-public-subnets-ids[0]]
  vpc_private_subnet_ids              = [module.f5-rs-aws-tgw-network.vpc-1-private-subnets-ids[0]]
  BIGIP_PWD                           = var.BIGIP_PWD
  providers = {
    aws     = aws
  }  
}

module "dev-docker-a" {
  source       = "git::https://gitlab.com/f5-examples/f5-examples-aws.git//terraform_modules/aws-docker-host"
#  source       = "../f5-examples-aws/terraform_modules/aws-docker-host"
  name         = "dev-docker-a"
  aws_region   = "us-west-2"
  username     = var.USERNAME
  key_name     = module.f5-rs-aws-tgw-network.key_name
  vpc_id       = module.f5-rs-aws-tgw-network.vpc-10-id
  subnet_id    = module.f5-rs-aws-tgw-network.vpc-10-subnets-ids[0]
  providers = {
    aws     = aws
  }  
}

module "dev-docker-b" {
  source       = "git::https://gitlab.com/f5-examples/f5-examples-aws.git//terraform_modules/aws-docker-host"
#  source       = "../f5-examples-aws/terraform_modules/aws-docker-host"
  name         = "dev-docker-b"
  aws_region   = "us-west-2"
  username     = var.USERNAME
  key_name     = module.f5-rs-aws-tgw-network.key_name
  vpc_id       = module.f5-rs-aws-tgw-network.vpc-10-id
  subnet_id    = module.f5-rs-aws-tgw-network.vpc-10-subnets-ids[1]
  providers = {
    aws     = aws
  }  
}

module bigip_onboard {
  source       = "git::https://gitlab.com/f5-examples/f5-examples-aws.git//terraform_modules/bigip-onboard"
#  source = "../f5-examples-aws/terraform_modules/bigip-onboard"
  bigip_address                       = module.bigip.mgmt_internet_ip
  bigip_username                      = "admin"
  bigip_password                      = var.BIGIP_PWD
  do_json = templatefile(
    "${path.module}/do.tpl",
    {
      hostname        = "bigip-a.example.com"
      private_ip      = "${module.bigip.private_addresses[0]}/24"
      public_ip       = "${module.bigip.public_addresses[0]}/24"
      advisory_text   = "BIGIP-A"
      internal_gw     = "${element(split(".", module.bigip.private_addresses[0]),0)}.${element(split(".", module.bigip.private_addresses[0]),1)}.${element(split(".", module.bigip.private_addresses[0]),2)}.1"
    }
  )
}

module bigip_as3 {
  source       = "git::https://gitlab.com/f5-examples/f5-examples-aws.git//terraform_modules/bigip-as3-tf"
  bigip_address                       = module.bigip.mgmt_internet_ip
  bigip_username                      = "admin"
  bigip_password                      = var.BIGIP_PWD
  as3_json = templatefile(
    "${path.module}/as3.tpl",
    {
      as3_sd_tag_value     = "juiceshop"
      vs_address           = module.bigip.public_addresses[0]
      as3_sd_aws_region    = "us-west-2"
    }
  )
  config_name  = "juiceshop1"
}